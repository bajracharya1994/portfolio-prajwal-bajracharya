$(document).ready(function () {
  $(".navigation--toggle").on("click", function () {
    $(".navigation--content").toggleClass("is--open");
    $(".content--wrapper").toggleClass("is--open");
    $("body").toggleClass("overflow-lock");
    $(this).toggleClass("is--open");
  });
  $('.navigation--item a').on('click', function () {
    $(".navigation--content").removeClass("is--open");
    $(".content--wrapper").removeClass("is--open");
    $("body").removeClass("overflow-lock");
    $('.navigation--toggle').removeClass("is--open");
  })
  AOS.init({
    duration: 1000,
    once: true
  });
  $('#scrollbar-outer').stellar();

  var navToggle = $(".navigation--toggle");
  var winHeight = document.documentElement.clientHeight;
  $('#scrollbar-outer').scroll(function () {
    var windowpos = $('#scrollbar-outer').scrollTop() + 50;
    if (windowpos >= winHeight) {
      navToggle.addClass("dark-nav");
    } else {
      navToggle.removeClass("dark-nav");
    }

  });
  var distance = $('.experience-education').offset().top - 50;
  $('#scrollbar-outer').scroll(function () {
    if ($(this).scrollTop() >= distance) {
      navToggle.addClass("footer-white-nav");
    } else {
      navToggle.removeClass("footer-white-nav");
    }
  });


  var winHeight = $(window).height(),
    docHeight = $(document).height(),
    progressBar = $(".scroll-indicator"),
    max,
    value;

  /* Set the max scrollable area */
  max = docHeight - winHeight;
  progressBar.attr("max", max);

  $(document).on("scroll", function () {
    value = $(window).scrollTop();
    progressBar.attr("value", value);
  });

  NodeCursor({
    cursor: true,
    node: true,
    cursor_velocity: 1,
    node_velocity: 0.15,
    native_cursor: 'none',
    element_to_hover: '.nodeHover',
    cursor_class_hover: 'disable',
    node_class_hover: 'expand',
    hide_mode: true,
    hide_timing: 2000,
  });


  ScrollReveal().reveal('.portfolio--description', {
    delay: 300,
    distance: '100px',
    easing: 'ease' ,
    duration: 1000,
    origin: 'right',
    container: '#scrollbar-outer',
    viewOffset: {
      top: 500
    },
  });

  ScrollReveal().reveal('.h-line', {
    delay: 350,
    distance: '100px',
    easing: 'ease' ,
    duration: 1000,
    origin: 'left',
    container: '#scrollbar-outer',
    viewOffset: {
      top: 500
    },
  });

  ScrollReveal().reveal('.portfolio--action a', {
    delay: 400,
    distance: '100px',
    easing: 'ease' ,
    duration: 1000,
    origin: 'bottom',
    container: '#scrollbar-outer',
    interval: 50,
    viewOffset: {
      top: 500
    },
  });

  ScrollReveal().reveal('.portfolio--image', {
    delay: 400,
    distance: '100px',
    easing: 'ease' ,
    duration: 1000,
    origin: 'right',
    container: '#scrollbar-outer',
    viewOffset: {
      top: 500
    },
  });

  ScrollReveal().reveal('.portfolio--title', {
    delay: 400,
    distance: '100px',
    easing: 'ease' ,
    duration: 1000,
    origin: 'left',
    container: '#scrollbar-outer',
    viewOffset: {
      top: 500
    },
  });
});

$(window).load(function () {
  // Animate loader off screen
  $(".page-loader").fadeOut("slow");
});
$(window).load(function () {
  // Animate loader off screen
  $(".page-loader").fadeOut("slow");
});

$(function () {
  initScrollToTop();
});
// End $(function)

function initScrollToTop() {
  //Check to see if the window is top if not then display button
  $(window).scroll(function () {
    var scrollTop = $(window).scrollTop(),
      docHeight = $(document).height(),
      winHeight = $(window).height(),
      scrollPercent = scrollTop / (docHeight - winHeight),
      scrollPercentRounded = Math.round(scrollPercent * 100);
    if (scrollPercentRounded > 15) {
      $(".topTop").css({
        opacity: 1,
        transform: "translateY(-50%)"
      });
    } else {
      $(".topTop").css({
        opacity: 0,
        transform: "translateY(50%)"
      });
    }
  });

  // Click event to scroll to top
  $(".topTop").click(function () {
    $("html, body").animate({
        scrollTop: 0
      },
      1200
    );
    return false;
  });
}