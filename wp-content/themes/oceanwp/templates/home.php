<?php

/**
 * Template Name: Home Page
 *
 * @package OceanWP WordPress theme
 */

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="title" content="<?php wp_title(); ?>" />
  <meta name="description" content="<?php bloginfo('description'); ?>" />
  <meta name="keywords" content="prajwal bajracharya, web designer, web designer nepal, image retoucher" />
  <meta name="robots" content="index, follow" />
  <meta name="language" content="English" />
  <meta name="author" content="Prajwal Bajracharya" />

  <meta name="google-site-verification" content="2zOooRrR99-Fcn1WzoBKyjwOhMcTUAyKfNthpYlbW_o" />

  <title><?php wp_title(); ?></title>
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/apple-touch-icon.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/favicon-16x16.png" />
  <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/site.webmanifest" />
  <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/safari-pinned-tab.svg" color="#5bbad5" />
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/favicon.ico" />
  <meta name="msapplication-TileColor" content="#da532c" />
  <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/dist/img/siteicon/browserconfig.xml" />
  <meta name="theme-color" content="#ffffff" />
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/dist/fonts/BuiltTitlingRg-Regular.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/dist/fonts/Goku-Regular.woff2" as="font" type="font/woff2" crossorigin>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/theme.css" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133161283-5"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-133161283-5');
  </script>

  <script data-ad-client="ca-pub-4050389918981173" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body class="">
  <div class="node" id="node"></div>
  <div class="cursor" id="cursor"></div>
  <progress value="0" class="scroll-indicator"></progress>
  <div class="page-loader">
    <div class="loadingio-spinner-eclipse-rh3hbvfw09h">
      <div class="ldio-d5jivk1ia27">
        <div></div>
      </div>
    </div>
  </div>
  <nav class="navigation font-display">
    <a href="javascript:void(0);" class="navigation--toggle nodeHover" data-aos="fade-left" data-aos-delay="350">
      <span></span>
      <span></span>
      <span></span>
    </a>
    <div class="navigation--content">
      <ul class="navigation--list">
        <li class="navigation--item link-effect nodeHover">
          <a class="text-white" href="index.html">Home</a>
        </li>
        <li class="navigation--item link-effect nodeHover">
          <a class="text-white" href="#portfolio">Portfolio</a>
        </li>
        <li class="navigation--item link-effect nodeHover">
          <a class="text-white" href="#exp-edu">Experience</a>
        </li>
        <li class="navigation--item link-effect nodeHover">
          <a class="text-white" href="<?php echo site_url('blog'); ?>">Blog</a>
        </li>
      </ul>
      <div class="navigation--info">
        <ul class="font-condensed">
          <li class="link-effect nodeHover">
            <a class="text-white" target="_blank" href="https://www.instagram.com/prajwalbajracharya/">Instagram</a>
          </li>
          <li class="link-effect nodeHover">
            <a class="text-white" target="_blank" href="https://www.linkedin.com/in/prajwalbajracharya/">LinkedIn</a>
          </li>
          <li class="link-effect nodeHover">
            <a class="text-white" target="_blank" href="https://www.pexels.com/@prajwalbajracharya">Pexels</a>
          </li>
          <li class="link-effect nodeHover">
            <a class="text-white" href="mailto:hello@prajwalbajracharya.com.np" target="_blank">hello@prajwalbajracharya.com.np</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="content--wrapper scrollbar-outer" id="scrollbar-outer">
    <section class="hero" id="home">
      <div class="hero--banner">
        <div class="hero--image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/img/hero3.jpg);" data-stellar-background-ratio="0.5">
          <!-- <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hero3.jpg" alt="" data-stellar-background-ratio="0.5"> -->
        </div>
        <div class="hero--info font-condensed">
          <span class="hero--name" data-aos="fade-right" data-aos-delay="300"><span>Prajwal Bajracharya</span></span>
          <span class="hero--designation" data-aos="fade-right" data-aos-delay="500"><span>Web Designer</span></span>
        </div>
        <div class="hero--content">
          <h1 class="hero--title text-secondary font-display" data-aos="fade-up" data-aos-delay="550">
            Prajwal
          </h1>
          <p class="hero--description" data-aos="fade-up" data-aos-delay="300">
            <span class="font-condensed text-uppercase">I'm a Web Designer</span>
            from Nepal and I work as a Frontend Designer at Leapfrog Technology. With a
            background in Management, my strength lies in website design.<br />
            To know more about me, my work or if you wants to offer me a job for
            a lot of money, feel free to contact me at
            hello@prajwalbajracharya.com.np
          </p>
        </div>
      </div>
      <span class="v-line" data-aos="fade-up" data-aos-delay="700"></span>
    </section>
    <div class="portfolio-wrapper" id="portfolio">
      <section class="portfolio energel-kawaii">
        <div class="portfolio--container portfolio--item">
          <div class="grid-x">
            <div class="portfolio--content cell medium-7">
              <h2 class="section--title portfolio--title font-display">
                Pentel Kawaii
              </h2>
              <div class="portfolio--detail">
                <span class="h-line"></span>
                <p class="portfolio--description">
                  A word that is known by the world, “Kawaii” is a term to
                  describe anything that holds significant value and or cherish
                  by one.<br />
                  Get ready to fall in love with its smooth writing Energel ink
                  and write to your heart’s content!
                </p>
                <div class="portfolio--action font-condensed">
                  <!-- <a href="javascript:void(0);" class="hollow button nodeHover secondary large">View Case</a> -->
                  <a href="http://energel-kawaii.com.sg" target="_blank" class="hollow button nodeHover primary large">Visit Website</a>
                </div>
              </div>
            </div>
            <div class="portfolio--image cell medium-5">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/pentel.png" alt="Pentel Kawaii" />
            </div>
          </div>
        </div>
      </section>
      <section class="portfolio samunnat-nepal">
        <div class="portfolio--container portfolio--item">
          <div class="grid-x">
            <div class="portfolio--content cell medium-7">
              <h2 class="section--title portfolio--title font-display">
                Samunnat Nepal
              </h2>
              <div class="portfolio--detail">
                <span class="h-line"></span>
                <p class="portfolio--description">
                  Samunnat Nepal (SN) is a national non-profit organization run by non-political social and development
                  professionals with an extensive experience working in the field of education, social and community
                  development
                </p>
                <div class="portfolio--action font-condensed">
                  <!-- <a href="javascript:void(0);" class="hollow button nodeHover secondary large">View Case</a> -->
                  <a href="http://samunnatnepal.org/" target="_blank" class="hollow button nodeHover primary large">Visit Website</a>
                </div>
              </div>
            </div>
            <div class="portfolio--image cell medium-5">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/samunnat.png" alt="Portfolio" />
            </div>
          </div>
        </div>
      </section>
      <section class="portfolio kan-yamada">
        <div class="portfolio--container portfolio--item">
          <div class="grid-x">
            <div class="portfolio--content cell medium-7">
              <h2 class="section--title portfolio--title font-display">
                Kan Yamada
              </h2>
              <div class="portfolio--detail">
                <span class="h-line"></span>
                <p class="portfolio--description">
                  A word that is known by the world, “Kawaii” is a term to
                  describe anything that holds significant value and or cherish
                  by one.<br />
                  Get ready to fall in love with its smooth writing Energel ink
                  and write to your heart’s content!
                </p>
                <div class="portfolio--action font-condensed">
                  <!-- <a href="javascript:void(0);" class="hollow button nodeHover secondary large">View Case</a> -->
                  <a href="http://cms.yigserver.com/kan-yamada/v0.1/" target="_blank" class="hollow button nodeHover primary large">Visit Website</a>
                </div>
              </div>
            </div>
            <div class="portfolio--image cell medium-5">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/kanyamada.png" alt="Portfolio" />
            </div>
          </div>
        </div>
      </section>
      <section class="portfolio easy-home">
        <div class="portfolio--container portfolio--item">
          <div class="grid-x">
            <div class="portfolio--content cell medium-7">
              <h2 class="section--title portfolio--title font-display">
                Easy Home
              </h2>
              <div class="portfolio--detail">
                <span class="h-line"></span>
                <p class="portfolio--description">
                  Easy Homes offer safe and spacious modern and traditional rooms in different locations with attached
                  and shared bathrooms, kitchen and other amenities such as better wifi services, hot shower, and 24 hrs
                  electricity backup.
                </p>
                <div class="portfolio--action font-condensed">
                  <!-- <a href="javascript:void(0);" class="hollow button nodeHover secondary large">View Case</a> -->
                  <a href="http://easyhome.com.np/" target="_blank" class="hollow button nodeHover primary large">Visit
                    Website</a>
                </div>
              </div>
            </div>
            <div class="portfolio--image cell medium-5">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/easyhome.png" alt="Portfolio" />
            </div>
          </div>
        </div>
      </section>
    </div>
    <section class="experience-education" id="exp-edu">
      <div class="exp-edu-container">
        <div class="grid-x">
          <div class="experience cell medium-6">
            <h2 class="section--title experience--title font-display">Experience</h2>
            <div class="experience-list">
              <ul>
                <li><span class="list-label">Web Designer</span><span class="list-data">Miracle Interface</span></li>
                <li><span class="list-label">Admin and Accountant</span><span class="list-data">Miracle Interface</span>
                </li>
                <li><span class="list-label">Internship</span><span class="list-data">Sunrise Bank</span></li>
              </ul>
            </div>
          </div>
          <div class="education cell medium-6">
            <h2 class="section--title education--title font-display">Education</h2>
            <div class="education-list">
              <ul>
                <li><span class="list-label">MBA</span><span class="list-data">DAV Business School</span></li>
                <li><span class="list-label">BBA</span><span class="list-data">Teerthanker Mahaveer University</span>
                </li>
                <li><span class="list-label">+2 </span><span class="list-data">Prasadi Academy</span></li>
                <li><span class="list-label">SLC</span><span class="list-data">Future Stars High School</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <span class="topTop"></span>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/jquery.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/jquery.migrate.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/nodeCursor.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/scrollReveal.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/stellar.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/aos.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/dist/js/custom.js"></script>
</body>

</html>